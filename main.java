package javaoop;

import java.util.ArrayList;
import java.util.Scanner;

public class main {

	static ArrayList<computer> brands = new ArrayList<>();

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner scn = new Scanner(System.in);

		computer c1 = new computer("asus",1);
		computer c2 = new computer("msi",2);
		computer c3 = new computer("acer",3);
		computer c4 = new computer("lenovo",4);

		brands.add(c1);
		brands.add(c2);
		brands.add(c3);
		brands.add(c4);

		System.out.println("computer : " + brands);
		System.out.println("input ID to delete : ");
		int computerId = scn.nextInt();

		for (int id = 0; id <= brands.size(); id++) {
			if (computerId == id) {
				brands.remove(id - 1);

				System.out.println("ID computer : " + computerId + " is deleted.");
				System.out.println("computer : " + brands);
			}
		}

	}

	public static void addID(int numberId) {
		for (int id = 0; id < numberId; id++) {
			brands.add(id + 1, null);
		}
		System.out.println("ID computer : " + numberId + " to add.");
		System.out.println("computer : " + brands);

	}

}


